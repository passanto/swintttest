package com.swintt.user.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swintt.user.dto.UserPostRequestDto;
import com.swintt.user.exception.NotFoundException;
import com.swintt.user.model.User;
import com.swintt.user.service.UserService;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.sql.SQLIntegrityConstraintViolationException;

import static java.lang.String.format;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    public static final String DATA_INTEGRITY_MSG = "could not execute statement; SQL [n/a]; constraint [users.user_name]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement";

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final Integer userId = 1;
    private final String userName = "john123";
    private final String realName = "John Smith";
    private final BigDecimal balance = BigDecimal.TEN;
    private final User userMock = new User(userId, userName, realName, balance);
    private final UserPostRequestDto userPostRequestDto = new UserPostRequestDto(userName, realName, balance);

    @Test
    @DisplayName("POST /user added")
    void testAddingUserSuccess() throws Exception {
        doReturn(userMock).when(userService).addUser(any());

        mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userPostRequestDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.userId", is(userId)))
                .andExpect(jsonPath("$.userName", is(userName)))
                .andExpect(jsonPath("$.realName", is(realName)));

    }

    @Test
    @DisplayName("POST /user not-added")
    void testAddingUserFail() throws Exception {
        SQLIntegrityConstraintViolationException sqlException = new SQLIntegrityConstraintViolationException();
        ConstraintViolationException cause = new ConstraintViolationException(
                "could not execute statement", sqlException, "users.user_name"
        );
        doThrow(new DataIntegrityViolationException(DATA_INTEGRITY_MSG, cause)).when(userService).addUser(any());

        mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userPostRequestDto)))
                .andExpect(status().isConflict());
    }

    @Test
    @DisplayName("GET /user found")
    void testGetUser() throws Exception {
        doReturn(userMock).when(userService).getUser(any(), any(), any());

        mockMvc.perform(get(format("/user?user-id=%s&user-name=%s&real-name=%s", userId, userName, realName))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userPostRequestDto)))
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.userId", is(userId)))
                .andExpect(jsonPath("$.userName", is(userName)))
                .andExpect(jsonPath("$.realName", is(realName)));
    }


    @Test
    @DisplayName("GET /user by user-id found")
    void testGetUserByUserId() throws Exception {
        doReturn(userMock).when(userService).getUser(any(), isNull(), isNull());

        mockMvc.perform(get(format("/user?user-id=%s", userId))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userPostRequestDto)))
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.userId", is(userId)))
                .andExpect(jsonPath("$.userName", is(userName)))
                .andExpect(jsonPath("$.realName", is(realName)));
    }

    @Test
    @DisplayName("GET /user by user-name found")
    void testGetUserByUserName() throws Exception {
        doReturn(userMock).when(userService).getUser(isNull(), any(), isNull());

        mockMvc.perform(get(format("/user?user-name=%s", userName))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userPostRequestDto)))
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.userId", is(userId)))
                .andExpect(jsonPath("$.userName", is(userName)))
                .andExpect(jsonPath("$.realName", is(realName)));
    }

    @Test
    @DisplayName("GET /user by real-name found")
    void testGetUserByRealName() throws Exception {
        doReturn(userMock).when(userService).getUser(isNull(), isNull(), any());

        mockMvc.perform(get(format("/user?real-name=%s", realName))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userPostRequestDto)))
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.userId", is(userId)))
                .andExpect(jsonPath("$.userName", is(userName)))
                .andExpect(jsonPath("$.realName", is(realName)));
    }

    @Test
    @DisplayName("GET /user by real-name not-found")
    void testGetUserNotFound() throws Exception {
        doThrow(NotFoundException.class).when(userService).getUser(isNull(), isNull(), any());

        mockMvc.perform(get(format("/user?real-name=%s", realName))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}