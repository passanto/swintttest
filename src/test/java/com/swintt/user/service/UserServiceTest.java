package com.swintt.user.service;

import com.swintt.user.dto.UserPostRequestDto;
import com.swintt.user.model.User;
import com.swintt.user.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class UserServiceTest {

    private final Integer userId = 1;
    private final String userName = "john123";
    private final String realName = "John Smith";
    private final BigDecimal balance = BigDecimal.TEN;
    private final User userMock = new User(userId, userName, realName, balance);
    private final UserPostRequestDto userPostRequestDto = new UserPostRequestDto(userName, realName, balance);

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @Test
    @DisplayName("Test addUser Success")
    void testAddingUserSuccess() {
        doReturn(userMock).when(userRepository).save(any());

        User user = userService.addUser(userPostRequestDto);

        Assertions.assertNotNull(user, "User added");
        Assertions.assertSame(user, userMock, "User added is the same");
    }

    @Test
    @DisplayName("Test addUser Fail")
    void testAddingUserFail() {
        doThrow(DataIntegrityViolationException.class).when(userRepository).save(any());

        Assertions.assertThrows(DataIntegrityViolationException.class, () -> userService.addUser(userPostRequestDto));
    }

    @Test
    @DisplayName("Test getUserByUserId Success")
    void testGetUserByUserId() {
        doReturn(Optional.of(userMock)).when(userRepository).findByUserIdAndUserNameAndRealName(userId, null, null);
        User user = userService.getUser(userId, null, null);

        Assertions.assertEquals(userMock, user);
    }

    @Test
    @DisplayName("Test getUserByUserName Success")
    void testGetUserByUserName() {
        doReturn(Optional.of(userMock)).when(userRepository).findByUserIdAndUserNameAndRealName(null, userName, null);
        User user = userService.getUser(null, userName, null);

        Assertions.assertEquals(userMock, user);
    }

    @Test
    @DisplayName("Test getUserByRealName Success")
    void testGetUserByRealName() {
        doReturn(Optional.of(userMock)).when(userRepository).findByUserIdAndUserNameAndRealName(null, null, realName);
        User user = userService.getUser(null, null, realName);

        Assertions.assertEquals(userMock, user);
    }
}