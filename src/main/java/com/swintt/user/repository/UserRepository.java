package com.swintt.user.repository;

import com.swintt.user.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {

    @Query("SELECT u " +
            "FROM User u " +
            "WHERE (:userId is null or u.userId = :userId) " +
            "and (:userName is null or u.userName = :userName)" +
            "and (:realName is null or u.realName = :realName)")
    Optional<User> findByUserIdAndUserNameAndRealName(Integer userId, String userName, String realName);

}
