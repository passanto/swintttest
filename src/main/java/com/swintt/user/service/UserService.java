package com.swintt.user.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swintt.user.dto.UserPostRequestDto;
import com.swintt.user.exception.NotFoundException;
import com.swintt.user.model.User;
import com.swintt.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static java.lang.String.format;

@Service
public class UserService {

    private UserRepository userRepository;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public User addUser(UserPostRequestDto userDto) {
        User user = objectMapper.convertValue(userDto, User.class);
        return userRepository.save(user);
    }

    public User getUser(Integer userId, String userName, String realName) {
        Optional<User> userOptional = userRepository.findByUserIdAndUserNameAndRealName(userId, userName, realName);
        return userOptional.orElseThrow(() -> new NotFoundException(format(
                "User with userId %s and userName %s and realName %s not found",
                userId, userName, realName)));
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

}
