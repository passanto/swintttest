package com.swintt.user.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swintt.user.dto.UserPostRequestDto;
import com.swintt.user.dto.UserResponseDto;
import com.swintt.user.exception.NotFoundException;
import com.swintt.user.model.User;
import com.swintt.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserResponseDto addUser(@RequestBody @Valid UserPostRequestDto userDto) {
        User user = userService.addUser(userDto);
        return objectMapper.convertValue(
                user,
                UserResponseDto.class
        );
    }

    @GetMapping
    @ResponseStatus(HttpStatus.FOUND)
    public UserResponseDto getUser(@RequestParam(name = "user-id", required = false) Integer userId,
                                   @RequestParam(name = "user-name", required = false) String userName,
                                   @RequestParam(name = "real-name", required = false) String realName)
            throws NotFoundException {
        User user = userService.getUser(userId, userName, realName);
        return objectMapper.convertValue(
                user,
                UserResponseDto.class
        );
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}
