package com.swintt.user.dto;

import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class UserPostRequestDto {

    @NotBlank(message = "UserName is a required field")
    private String userName;

    @NotBlank(message = "RealName is a required field")
    private String realName;

    @NumberFormat(style = NumberFormat.Style.CURRENCY)
    @NotNull(message = "Balance is a required field")
    private BigDecimal balance;

    public UserPostRequestDto(String userName, String realName, BigDecimal balance) {
        this.userName = userName;
        this.realName = realName;
        this.balance = balance;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
