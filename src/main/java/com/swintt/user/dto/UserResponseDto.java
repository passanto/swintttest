package com.swintt.user.dto;

import java.math.BigDecimal;

public class UserResponseDto {

    private Integer userId;
    private String userName;
    private String realName;
    private BigDecimal balance;

    public UserResponseDto() {
    }

    public UserResponseDto(Integer userId, String userName, String realName, BigDecimal balance) {
        this.userId = userId;
        this.userName = userName;
        this.realName = realName;
        this.balance = balance;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
