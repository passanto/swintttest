# Getting Started

### Task description

Create a simple REST api server in Java, operating on port 80 (also show the code for a SSL version operating on 443).

* The server should connect to a mysql database on localhost with the username "root” and password "
  mysql” (https://ampps.com/ - default installation).
* The API should have a method for storing a user record with "username”, "real name” and "balance” taking json as
  input.
* The API should have a method for retrieving a user record, deliverying json as output.
* Create the DB schema.
* Document the schema, code and json.
* Deliver the code sample, and a compiled runnable with step by step instructions on how to install and run on
  localhost, and any requirements.

***

### Requirements

* Open JDK 16
* Maven 3.8.3
* Docker

***

### Starting the MySql docker container via bash

```shell
docker run --name mysql-latest \
-p 3306:3306 -p 33060:33060 \
-e MYSQL_ROOT_PASSWORD=mysql \
-e MYSQL_ROOT_HOST=% \
-d mysql/mysql-server:latest
```

### Test the SpringBoot app

```shell
mvn test
```

### Starting the SpringBoot app (default profile)

From the root of the project:

```shell
mvn spring-boot:run
```

This enables the *default* profile described by the **application.yml** file.  
It runs the app on the standard Spring MVC port (8080).

##### Web profile

The app can be served on port 80 running the following command as a *sudoers* user

```shell
mvn spring-boot:run -Dspring-boot.run.profiles=web
```

This enables the *web* profile described by the **application-web.yml** file.

##### TLS profile

The app can be served on port 443, with the provided self signed PFX file  **
/src/main/resources/keystore/keystore.p12**.

```shell
mvn spring-boot:run -Dspring-boot.run.profiles=tls
```

This enables the *tls* profile described by the **application-tls.yml** file.

There are various options in order to run the server on port 80 or 443 there:

* run the app as a *sudoers* user
* assign the *CAP_NET_BIND_SERVICE* capability to the Java executable
* forward requests to the app running on a higher port (ie: 8080)

***

### Application architecture

The application relies on the standard **SpringBoot** layering architecture:

* REST controller
* Service layer
* JPA persistence layer
* MySQL datasource

### Application behaviour

**SpringBoot** automatically creates the schema to create the required schema if required (ie: first run).  
**Flyway** is responsible to run the initial DDL scripts. For testing purposes the **H2** in memory database is
being used.  
In a more complex scenario **Testcontainers** would be a better fit.  
The **user-requests.http** can be used to send REST calls to the app via IntelliJ.
***

#### POST method

Clients are allowed to call the POST method in order to create users given a valid JSON request:

```json
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "userName": {
      "type": "string"
    },
    "realName": {
      "type": "string"
    },
    "balance": {
      "type": "number"
    }
  },
  "required": [
    "userName",
    "realName",
    "balance"
  ]
}
```

example:

```shell
curl -X POST \
  -H "Content-type: application/json"\
  --data '{"userName":"userName","realName":"Real Name","balance":0.0}' \
  http://localhost:8080/swintt/user
```

#### GET method

Clients are allowed to call the GET method in order to retrieve a user given at least one of the attributes:

```shell
* /user/?user-id={userId}&user-name={userName}&real-name={realName}
```

example:

```shell
curl -X GET \
  http://localhost:8080/swintt/user?user-id=1
```