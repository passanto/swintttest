# Comments about the CompleteWinXfers class
### createInstance
The **createInstance** method seems to unnecessarily synchronise part of its execution.  

Being **final**, the **_singleton** Class variable will be the same for all the invoking callees, and won't be changed once assigned. 

Assuming the **getSingleton** methods is being called concurrently, whichever calling thread will manage to assign the **_singleton** attribute, will correctly assign a *Ready* *daemon* thread.  

Furthermore, synchronisation mechanism are intended to prevent concurrency issue related to shared variables (instance variables), whereas **i** is a local one.  

Same applies for the **addXferLog** and **addXferLogs** methods, where the synchronised block could only wrap the access to the shared **_xferLogs** vector.  
Moreover, *Vector* is a thread safe class.